//Starting in the top left corner of a 2×2 grid, and only being able to move to the 
//right and down, there are exactly 6 routes to the bottom right corner.
// x x x 1
// x x x 2
// x x x 
// 1 2
//
// x x x x 1
// x x x x 3*1
// x x x x 3+
// x x x x
// 1 2 3 
//
// 20+19+18
//How many such routes are there through a 20×20 grid?
let g = 21
let firstRow = [|for _ in [1..g] -> 1L |]

let rec nextRow (prevRow:int64 array) index prev =
    let addNext index prev =
        match prev with
        | [] -> [1L]
        | head::tail -> head + prevRow.[index] :: prev

    match index with 
    | 21 -> prev
    | _ -> nextRow prevRow (index+1) (addNext index prev)
let rec someRows prevRows count =
    match count = (List.length prevRows) with
    | true -> prevRows
    | false -> 
                let next = nextRow (List.head prevRows) 0 []
                let next' = next |> List.rev |> List.toArray
                someRows (next' :: prevRows) (count)
let grid = (someRows [firstRow] g)
let printRow (r:int64 array) = r |> Array.iter (printf "%15d")
let printRow' r =
    printRow r
    printfn ""
grid |> List.iter printRow'

