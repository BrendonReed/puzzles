//If the numbers 1 to 5 are written out in words: 
//one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
//
//If all the numbers from 1 to 1000 (one thousand) inclusive were 
//written out in words, how many letters would be used?
//
//
//NOTE: Do not count spaces or hyphens. 
//For example, 342 (three hundred and forty-two) contains 23 letters 
//and 115 (one hundred and fifteen) contains 20 letters. 
//The use of "and" when writing out numbers is in compliance with British usage.
//
open System
let onesM = 
    [   0, ""; 
        1, "one";
        2, "two";
        3, "three";
        4, "four";
        5, "five";
        6, "six";
        7, "seven";
        8, "eight";
        9, "nine";
        10, "ten";
        11, "eleven";
        12, "twelve";
        13, "thirteen";
        14, "fourteen";
        15, "fifteen";
        16, "sixteen";
        17, "seventeen";
        18, "eighteen";
        19, "nineteen";
        20, "twenty"; ]
    |> Map.ofList
let tensM = 
    [   2, "twenty";
        3, "thirty";
        4, "forty";
        5, "fifty";
        6, "sixty";
        7, "seventy";
        8, "eighty";
        9, "ninety"; ]
    |> Map.ofList
   
let wordify n = 
    let asString = n.ToString().ToCharArray() 
                |> Array.map(fun i -> 
                            Int32.Parse(i.ToString()))
    let concat a b = Int32.Parse(a.ToString() + b.ToString())
    match asString with 
    | [||] -> ""
    | [| ones |] -> onesM.[ones]
    | [| tens; ones |] when tens < 2 -> onesM.[(concat tens ones)]
    | [| tens; ones |] -> tensM.[tens] + onesM.[ones]
    | [| hundreds; 0; 0 |] -> onesM.[hundreds] + "hundred"
    | [| hundreds; tens; ones |] when tens < 2 -> 
        onesM.[hundreds] + "hundredand" + onesM.[(concat tens ones)]
    | [| hundreds; tens; ones |] -> onesM.[hundreds] + "hundredand" + tensM.[tens] + onesM.[ones]
    | _ -> failwith "over 999 not supported"
let sum (s:string) = s.Length
printfn "%A %A" (wordify 342) (sum (wordify 342))
printfn "%A %A" (wordify 115) (sum (wordify 115))
printfn "%A %A" (wordify 100) (sum (wordify 100))
let sumall = [1..999] |> List.map wordify |> List.fold (+) ""
printfn "%A" ((sumall + "onethousand").ToCharArray()).Length
