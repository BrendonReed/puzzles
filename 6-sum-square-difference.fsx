//The sum of the squares of the first ten natural numbers is,
//
//1^2 + 2^2 + ... + 10^2 = 385
//The square of the sum of the first ten natural numbers is,
//
//(1 + 2 + ... + 10)^2 = 55^2 = 3025
//Hence the difference between the sum of the squares of the first ten natural 
//numbers and the square of the sum is 3025 − 385 = 2640.
//
//Find the difference between the sum of the squares of the 
//first one hundred natural numbers and the square of the sum.
let s = [1.0..100.0] |> List.fold (+) 0.0
let sumOfSquares = s **2.0
printfn "%A" sumOfSquares

let squareOfSums = [1.0..100.0] |> List.map (fun i -> i ** 2.0) |> List.fold (+) 0.0
printfn "%A" squareOfSums

printfn "%A" (sumOfSquares - squareOfSums)
