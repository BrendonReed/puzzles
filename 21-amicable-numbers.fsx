//Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
//If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and 
//each of a and b are called amicable numbers.
//
//For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; 
//therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
//
//Evaluate the sum of all the amicable numbers under 10000.
//proper = not including n
let findProperDivisors n =
    let max = int (sqrt (float n))
    let divsPair i =
        match n % i with | 0 -> [i; n / i] | _ -> []
    1::([2..max] |> List.map divsPair |> List.concat)

let sumDivs l = 
    l |> List.map (fun i -> (i,(findProperDivisors i)))
    |> List.map (fun (i,l) -> (i,List.sum l))
let x = [2..10000] |> sumDivs
let y = x |> List.map (fun (a,b) -> (b,a))
let z = 
    x 
    |> List.filter (fun i -> (List.exists (fun j -> j = i) y))
    |> List.filter (fun (x,y) -> x <> y)
    |> List.map (fun (x,y) -> x)
    |> List.sum
//printfn "%A" x
//printfn "%A" y
printfn "%A" (z )
