//In the 20×20 grid below, four numbers along a diagonal line have been marked in red.
//
//The product of these numbers is 26 × 63 × 78 × 14 = 1788696.
//
//What is the greatest product of four adjacent numbers in the same direction 
//(up, down, left, right, or diagonally) in the 20×20 grid?
open System
open System.IO
#time
let gridF = File.ReadLines("11-grid.txt") |> Seq.toArray
let grid = 
    let splitToInt (s:string) = s.Split() |> Array.map int
    gridF |> Array.map splitToInt
let mutable max = 0
for r in [0..grid.Length-1-3] do
    let colLength = grid.[r].Length - 1 - 3
    for c in [0..colLength] do
        //sum 4 in all directions
        let row = grid.[r]
        let horiz = row.[c..c+3] |> Array.fold (*) 1
        max <- Math.Max(max, horiz)
        let vert = [for i in [r..r+3] -> grid.[i].[c]] |> List.fold (*) 1
        max <- Math.Max(max, vert)
        let diagDown = [0..3] |> List.map (fun n -> grid.[r+n].[c+n]) |> List.fold (*) 1
        max <- Math.Max(max, diagDown)
        //printf " %2i" grid.[r].[c]

for r in [3..grid.Length - 1] do
    for c in [0..(grid.[r].Length-1-3)] do
        let diagUp = [0..3] |> List.map (fun n -> grid.[r-n].[c+n]) |> List.fold (*) 1
        max <- Math.Max(max, diagUp)
printfn "%A" max
#time
