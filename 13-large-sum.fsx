//Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
open System
open System.IO
open System.Numerics
let digitsF = File.ReadLines("13-50-digit-numbers.txt")
let asStrings = digitsF |> Seq.map (fun i -> BigInteger.Parse(i)) |> Seq.toList 
//let numbers = 
//    digitsF 
//        |> Seq.map (fun i -> i.ToCharArray() 
//                            |> Array.map (fun i -> Int32.Parse(i.ToString())))
//        |> Seq.toList
//let columns = 
//    [|0..49|] 
//    |> Array.map (fun j -> (numbers |> List.map (fun i -> i.[j])))
//    |> Array.map (fun column -> Seq.fold (+) 0 column)
let mutable total = new BigInteger(0)
let sum = asStrings |> List.iter (fun i -> total <- BigInteger.Add(total, i))
printfn "%A" total
