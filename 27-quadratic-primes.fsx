//Euler discovered the remarkable quadratic formula:
//
//n² + n + 41
//
//It turns out that the formula will produce 40 primes for the consecutive 
//values n = 0 to 39. However, when n = 40, 40*2 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, 
//and certainly when n = 41, 41² + 41 + 41 is clearly divisible by 41.
//
//The incredible formula  n² − 79n + 1601 was discovered, which produces 80 primes 
//for the consecutive values n = 0 to 79. The product of the 
//coefficients, −79 and 1601, is −126479.
//
//Considering quadratics of the form:
//
//n² + an + b, where |a| < 1000 and |b| < 1000
//
//where |n| is the modulus/absolute value of n
//e.g. |11| = 11 and |−4| = 4
//Find the product of the coefficients, a and b, for the quadratic expression 
//that produces the maximum number of primes for consecutive values of n, starting with n = 0.
#time
let isprime n =
    let rec check i =
        i > n/2L || (n % i <> 0L && check (i + 1L))
    n > 0L && check 2L

let quad a b =
    let result n = 
        (n * n) + (a*n) + b
    let rec max n =
        match (isprime (result n)) with 
        | false when n=0L -> n
        | false -> n-1L
        | true -> max (n+1L)
    max 0L
let l = [-1000L..1000L]
//let l = [0L..100L]
[for x in l do
 for y in l do
    yield (x, y, (quad x y), x*y)]
|> List.maxBy (fun (x, y, q, p) -> q)
|> printfn "%A" 

//printfn "%A" (quad -79L 1601L)
//printfn "%A" (quad 1L 41L)
#time
