/*The number 3797 has an interesting property. 

Being prime itself, it is possible to continuously remove digits from left to right,
 and remain prime at each stage: 3797, 797, 97, and 7.

Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
*/

import { PrimeHelper } from './primes-sieve.mjs';

//just try it
let primeHelper = PrimeHelper(100000);
let isPrime = i => primeHelper.isPrime(i);

function isTruncatableLeft(number) {
    let truncatedLeft = number.toString().slice(1);
    while(truncatedLeft.length > 0) {
        if (!isPrime(parseInt(truncatedLeft, 10))) {
            return false;
        }
        truncatedLeft = truncatedLeft.toString().slice(1);
    }
    return true;
}

function isTruncatableRight(number) {
    let asString = number.toString();
    let truncatedRight = asString.slice(0, asString.length - 1);
    while(truncatedRight.length > 0) {
        if (!isPrime(parseInt(truncatedRight, 10))) {
            return false;
        }
        let asString = truncatedRight.toString();
        truncatedRight = asString.slice(0, asString.length - 1);
    }
    return true;
}

let results = [];
primeHelper.primesToCheck.forEach((item) => {
    if (item.toString().length !== 1 && isTruncatableLeft(item) && isTruncatableRight(item)) {
        results.push(item);
        console.log(results);
    }
});
let sum = results.reduce((acc, c) => acc + c);
console.log(sum);
