//A permutation is an ordered arrangement of objects. For example, 3124 is one 
//possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are 
//listed numerically or alphabetically, we call it lexicographic order. The 
//lexicographic permutations of 0, 1 and 2 are:
//
//012   021   102   120   201   210
//
//What is the millionth lexicographic permutation of the digits 
//0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
// 012 021
// 102 012 021
// 120
//
// take off the front element, append it on the front of each permutation of
// the tail
let putFront oList i = 
    i::Set.toList (Set.difference (Set.ofSeq oList) (Set.ofSeq [i]))
let eachInFront l =
    l |> List.map (fun i -> putFront l i)

let l = [0..2]
let rec perms2 aList = 
    match aList with 
    | [] -> [[]]
    | l -> 
        (eachInFront l) //012, 102, 201
        |> List.map (fun (x::rest) ->
             let p = perms2 rest
             p |> List.map (fun l -> x::l))
        |> List.concat

let x = (perms2 l) |> List.toArray 
x |> Array.iter (printfn "%A" )

