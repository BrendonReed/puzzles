open System
let rng = new Random()
let rec makePerm input output =
    match Set.count input with 
    | 0 -> output
    | _ -> 
        let index = rng.Next(Set.count input)
        let i = (input |> Set.toArray).[index]
        let output' = i::output
        makePerm (Set.difference input (Set.ofSeq output')) output'

let checkDiv x = 
    //x is a 10 digit list, check if eachx is divisible by n
    //is head divisible by 1?
    let rec foo acc count l =
        match l with
        | h::tail -> 
            let newAcc = acc + h.ToString()
            (int newAcc) % count = 0 && foo newAcc (count + 1) tail
        | [] -> ((int acc) % count) = 0
    foo "" 1 x


let allPerms input =
    let total =  List.fold (*) 1 [1..(Set.count input)] //calculate factorial
    let rec loop input output = 
        match Set.count output = total with
        | true -> 
            if (checkDiv (Set.toList output)) then printfn "%A" output
            output
        | false -> 
            let next = makePerm input []
            loop input (Set.add next output)
    loop input (Set.ofSeq [])
let a = (allPerms (Set.ofSeq [1..9])) 
printfn "made them all"
printfn "%A" (a |> Set.filter checkDiv)
a |> Set.iter (fun i -> (printfn "%A %A" i (checkDiv i)))

printfn "%A" (checkDiv [1; 2; 0])
// 2,3 -> 23, 32
// 1, 2,3 -> 123,213,231, 132,312,321
// 1, 2,3 -> 123, 213, 231, 132, 312, 321
// 0,1,2,3 -> 0123, 0132
// 1,2 -> 12, 21
// 0,1,2 -> 012, 021
