//Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing 
//over five-thousand first names, begin by sorting it into alphabetical order. 
//Then working out the alphabetical value for each name, multiply this value by its 
//alphabetical position in the list to obtain a name score.
//
//For example, when the list is sorted into alphabetical order, 
//COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. 
//So, COLIN would obtain a score of 938 × 53 = 49714.
//
//What is the total of all the name scores in the file?

open System.IO
let wordScore (n:string) = 
    n.ToCharArray() 
    |> Array.map (fun i -> (int i) - 64)
    |> Array.sum
let words = 
    (File.ReadAllText("p022_names.txt")).Split(',') 
    |> Array.map (fun i -> i.Replace("\"", ""))
    |> Array.sort
    |> Array.map wordScore
//printfn "%A" (words.Length)
//let lastIndex = ((words.Length) -1)
let total = 
    [1..(words.Length)] 
    |> List.map (fun i -> (words.[i-1] * i)) 
    |> List.sum
printfn "%A" total
