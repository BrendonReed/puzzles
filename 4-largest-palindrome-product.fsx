//A palindromic number reads the same both ways. 
//The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

//Find the largest palindrome made from the product of two 3-digit numbers.
//
let prod i =
    [100..999] |> List.map (fun x -> x * i)

let isPalindrome number = 
    let s = number.ToString()
    let middle = s.Length / 2
    let half1 = s.Substring(0, middle)
    let half2 = s.Substring(middle)
    let half2ReversedB = Array.rev (System.Text.Encoding.ASCII.GetBytes(half2))
    let half2Reversed = System.Text.Encoding.ASCII.GetString(half2ReversedB)
    half1 = half2Reversed

let palindromes = 
    [100..999] 
    |> List.map prod 
    |> List.concat 
    |> List.filter isPalindrome

printfn "%A" (List.max palindromes)

