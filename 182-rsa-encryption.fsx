open System.Linq

let p = 1009
let q = 3643
let n = p * q
let phi = (p - 1) * (q - 1)

let gcd a b =
    let rec gcdH a b =
        match (a, b) with
        | (0, r) -> r
        | (r, 0) -> r
        | (x, y) when x > y -> gcdH (x-y) y
        | (x, y) -> gcdH x (y-x)
    let r = gcdH a b
    (a, b, r)

let allE phi = 
    [1..phi] 
    |> List.map (gcd phi) 
    |> List.filter (fun (a, b, gcd) -> (gcd = 1))
    |> List.map (fun (a, b, gcd) -> b)

let encrypt (n:int) (e:int) (m:int) = 
    //c = m^e mod n
    let (b:bigint) = (bigint m) ** e // pown (bigint m) e
    let x = b % (bigint n)
    (m, x)

let unconcealed p q (e) = 
    let (_, _, gcdp) = gcd (e - 1) (p - 1)
    let (_, _, gcdq) = gcd (e - 1) (q - 1)
    let count = (1 + gcdp) * (1 + gcdq) //but why?
    (e, count)

let alle = allE phi
let unconcealedCount alle n =
    let alleCounts = alle |> List.map (unconcealed p q) 
    let (e, min) = alleCounts |> List.minBy (fun (e, count) -> count)

    alleCounts 
    |> List.filter (fun (e, count) -> count = min)
    |> List.sumBy (fun (e, count) -> bigint e)

printfn "%A" (unconcealedCount alle n)
