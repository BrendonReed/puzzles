//The following iterative sequence is defined for the set of positive integers:
//
//n → n/2 (n is even)
//n → 3n + 1 (n is odd)
//
//Using the rule above and starting with 13, we generate the following sequence:
//
//13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
//It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
//
//Which starting number, under one million, produces the longest chain?
//
//NOTE: Once the chain starts the terms are allowed to go above one million.

//This could be faster by saving the results, and lookup the count for that
//starting number each time we come across one we've already calculated
#time
let collatzeTerm n =
    match n%2L with
    | 0L -> n/2L
    | _ -> (3L*n) + 1L

let rec collatzeSeq prev length = 
    match prev with
    | 1L -> length
    | _ -> collatzeSeq (collatzeTerm prev) (length + 1L)

let mutable maxSoFar = 0L
let mutable numberCausedMax = 0L
for i in [1L..1000000L] do
    let c = collatzeSeq i 1L
    if c > maxSoFar then
        maxSoFar <- c
        numberCausedMax <- i

printfn "%A" numberCausedMax

#time
