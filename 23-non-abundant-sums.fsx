//A perfect number is a number for which the sum of its proper divisors is 
//exactly equal to the number. For example, the sum of the 
//proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, 
//which means that 28 is a perfect number.
//
//A number n is called deficient if the sum of its proper divisors is less than n 
//and it is called abundant if this sum exceeds n.
//
//As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, 
//the smallest number that can be written as the sum of two abundant numbers is 24. 
//By mathematical analysis, it can be shown that all integers greater than 28123 
//can be written as the sum of two abundant numbers. 
//However, this upper limit cannot be reduced any further by analysis even though 
//it is known that the greatest number that cannot be expressed as the 
//sum of two abundant numbers is less than this limit.
//
//Find the sum of all the positive integers which cannot be written as the sum of 
//two abundant numbers.
open System.Collections.Generic
#time
let findProperDivisors n =
    let max = int (sqrt (float n))
    let divsPair i =
        match n % i with 
        | 0 -> 
            let pair = n/i
            match pair = i with 
            | false -> [i; pair] 
            | true -> [i;]
        | _ -> []
    let divs = 
        [2..max] 
        |> List.map divsPair 
        |> List.concat
    (n, 1::divs)

let abundants = 
    [12..28123]
    |> List.map findProperDivisors
    |> List.map (fun (i, l) -> i, (List.sum l))
    |> List.filter (fun (i, s) -> s > i)
    |> List.map (fun (i,s) -> i)
//find all sums of 2 abundant numbers, then find all numbers below 28123 not in
//the list
//add each number in the list with every other number
let abundantSumsHash = new HashSet<int>()
abundants
|> List.map (fun i -> (abundants |> List.map (fun j -> j + i)))
|> List.concat
|> List.iter (fun i -> (abundantSumsHash.Add(i) |> ignore))

let others = 
    [1..28123] 
    |> List.filter (fun i -> not (abundantSumsHash.Contains(i)))
    |> List.sum
printfn "%A" others
#time
