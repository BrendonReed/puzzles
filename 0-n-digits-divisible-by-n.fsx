#time
let putFront oList i = 
    i::Set.toList (Set.difference (Set.ofSeq oList) (Set.ofSeq [i]))
let eachInFront l =
    l |> List.map (fun i -> putFront l i)

let checkDiv x = 
    //x is a 10 digit list, check if eachx is divisible by n
    //is head divisible by 1?
    let rec foo acc (count:int64) l =
        match l with
        | h::tail -> 
            let newAcc = acc + h.ToString()
            let divisible = (int64 newAcc) % count = 0L
            //printfn "%A %A" newAcc divisible
            divisible && foo newAcc (count + 1L) tail
        | [] -> ((int64 acc) % (count - 1L)) = 0L
    foo "" 1L x

let l = [1..9]
let rec perms2 aList = 
    match aList with 
    | [] -> [[]]
    | l -> 
        (eachInFront l) //012, 102, 201
        |> List.map (fun (x::rest) ->
             let p = perms2 rest
             p |> List.map (fun l -> x::l))
        |> List.concat

//printfn "%A" (checkDiv [1;0;2])
let x = (perms2 l) |> List.toArray 
x |> Array.filter checkDiv |> printfn "%A"
#time
