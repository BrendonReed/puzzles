﻿//In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
//    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
//It is possible to make £2 in the following way:
//    1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
//How many different ways can £2 be made using any number of coins?
//E2
//E1,E1
//E1, P50, P50
//E1, P50, P20, P20, P10
//E1, P50, P20, P20, P5, P5
//E1, P50, P20, P20, P5, P2, P2, P1
//E1, P50, P20, P20, P5, P2, P1, P1, P1
//E1, P50, P20, P20, P5, P1, P1, P1, P1, P1

//5
//2,2,1
//2, 1,1, 1
//1,1, 1,1, 1

//give 5, 2, 1
let rec foo n (soFar: string list list) current options : string list list =
    match options with
    | [] -> soFar
    | head::tail ->
        let biggestR = ("P" + head.ToString())
        if head = n then
            let finished = biggestR :: current
            foo n (finished :: soFar) [] tail
        elif n > head then
            //try head as many times as possible
            let fitsTimes = (n) / head
            let mults = ([1..fitsTimes] |> List.map (fun i -> biggestR))
            let newCurrent = mults @ current
            printfn "mults %A" newCurrent
            foo (n - (fitsTimes * head)) soFar newCurrent options
        else
            //remove front of list, try again
            let o = foo n soFar current tail
            o

let rec bar n (coins:int list) (current:int list) (winners:int list list) : int list list =
    match coins with
    | [] -> winners
    | head::tail ->
        let r = 
            coins
            |> List.collect (fun c ->
                let newCurrent = c :: current
                if (List.sum newCurrent) = n then
                    newCurrent :: winners
                elif (List.sum newCurrent) > n then
                    bar n (List.tail coins) current winners
                else
                    bar (n - c) coins newCurrent winners
                )
        r
    //for each coin, add it to current
    //if current passes (sum = n) add to winners
    //if current fails, take coin off
    //if current partial, 


//[["P1"; "P1"; "P1"; "P1"; "P1"]; ["P1"; "P2"; "P2"]; ["P5"]]
let main () =
    printfn "%A" (bar 5 [] [] [5; 2; 1])
