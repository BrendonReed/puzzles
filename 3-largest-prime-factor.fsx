//The prime factors of 13195 are 5, 7, 13 and 29.
//
//What is the largest prime factor of the number 600851475143 ?

//let n = 13195L
let n = 600851475143L

let checkPrime n = 
    let rec checkPrimeH n (i:int64) = 
        match i > (n / 2L) with 
        | true -> true
        | false ->
            match (n % i) = 0L with
            | true -> false
            | false -> checkPrimeH n (i+1L)
    checkPrimeH n 2L

let rec nextPrime prev =
    let next = prev + 2L
    match (checkPrime next) with 
    | true -> next
    | false -> nextPrime next

//attempt to divide n by a prime
//add it to the list if it divides, otherwise try the next prime
let rec primeFactors prime number result =
    let total = result |> Seq.fold (*) 1L
    match total = number with
    | true -> result
    | false ->
        let r = number % prime
        match r with 
        | 0L -> primeFactors (nextPrime prime) number (prime :: result)
        | _ -> primeFactors (nextPrime prime) number result

//printfn "%A" (primeFactors 3L n [])
let rec nthPrime n prev count = 
    match count = n with
    | true -> prev
    | false -> nthPrime n (nextPrime prev) (count+1L)
printfn "%A" (nthPrime 10001L 3L 2L)

