//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

let rec divisibleByAll number all =
    match all with 
    | [] -> true
    | head :: tail -> 
        match number % head = 0 with 
        | false -> false
        | true -> divisibleByAll number tail

let rec smallest n inc l =
    match divisibleByAll n l with
    | true -> n
    | false -> smallest (n+inc) inc l

printfn "%A" (smallest 20 20 [1..20])
