//You are given the following information, but you may prefer to do some research for yourself.
//
//1 Jan 1900 was a Monday.
//Thirty days has September,
//April, June and November.
//All the rest have thirty-one,
//Saving February alone,
//Which has twenty-eight, rain or shine.
//And on leap years, twenty-nine.
//A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
//How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
open System

let s = new DateTimeOffset(1901, 1, 1, 0, 0, 0, 0,TimeSpan.Zero)
let e = new DateTimeOffset(2000, 12, 31, 0, 0, 0, 0,TimeSpan.Zero)

let rec allDays (acc: DateTimeOffset list) = 
    match acc.Head = e with
    | true -> acc
    | false -> allDays ((acc.Head).AddDays(1.0) :: acc)

let r = (allDays [s]) |> List.filter (fun d -> d.Day = 1) |> List.filter (fun d -> d.DayOfWeek = DayOfWeek.Sunday)
printfn "%A" (r.Length)
