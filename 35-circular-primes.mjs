/*
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
*/
import { PrimeHelper } from "./primes-sieve.mjs"

function rotate(numberS) {
    let rotated = numberS.slice(1) + numberS[0];
    return rotated;
}
function allRotations(number, result) {
    let rotated = rotate(number.toString());
    if (result.find(i => i === rotated)) {
        return result;
    }
    else {
        return allRotations(rotated, result.concat([rotated]));
    }
}

function isCircularPrime(i, primes) {
    let rotations = allRotations(i, []);
    return rotations.every(j => primes.includes(Number(j)));
}
function under100Test() {
    let primeSieve = PrimeHelper(100);
    let isCircularPrimeHelper = function(i) { return isCircularPrime(i, primeSieve.primesToCheck)}
    let under100 = primeSieve.primesToCheck.filter(isCircularPrimeHelper);
    console.log(under100.length === 13 ? "Passed!": "Failed");
}
under100Test();
//tests
let a = allRotations(197, [])
let ar = [ '971','719','197' ];
console.log(a.every((v, ix) => v === ar[ix] ) ? "Passed": "Fail " + a);
let b = allRotations(101, [])
let br = [ "011", "110","101" ];
console.log(b.every((v, ix) => v === br[ix] ) ? "Passed": "Fail " + b);

function main() {
    let primeSieve = PrimeHelper(1000000);
    let isCircularPrimeHelper = function(i) { return isCircularPrime(i, primeSieve.primesToCheck)}
    let under = primeSieve.primesToCheck.filter(isCircularPrimeHelper);
    console.log(under.length);
}
main();