//A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
//
//a2 + b2 = c2
//For example, 32 + 42 = 9 + 16 = 25 = 52.
//
//There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//Find the product abc.
//for squares of c, find natuarl squares a^2 + b^2
let findSides c =
    let c' = c*c
    let asqs = [1..c-1] |> List.map (fun a -> a*a)
    let findB a2 =
        let diff = c' - a2
        let root = sqrt (float diff)
        let perfectSquare = (root - float (int root)) = 0.0
        match perfectSquare with 
        | true -> Some (((sqrt (float a2)), root), c)
        | false -> None
    let options = asqs |> List.map findB
    options 
    |> List.filter Option.isSome 
    |> List.map (fun o -> o.Value) 
    |> List.filter (fun ((a, b),c) -> a < b)
let sides = [100..500] |> List.map findSides |> List.concat 
let sums = sides |> List.map (fun ((a,b),c) -> (a+b+float c, (a,b,c)))
printfn "%A" sums
printfn "%A" (sums |> List.filter (fun (a, _) -> a = 1000.0))
    
