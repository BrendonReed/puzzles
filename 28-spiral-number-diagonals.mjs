/*
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
*/

/*
edge | total | diagonals | 
3 | 9   | 3,5,7,9 - 2
5 | 25  | 13,17,21,25 - 4
7 | 49  | 31,37,43,49 - 6
...  t . 
1001 | 1002001

...
4 new diagonals per ring
the 4 are sideLength - 1 apart, 
diagonals go up by 

*/

function calcDiagonals(sideLength) {
    let diagonals = [1];
    let lastDiagonal = 1;
    for(let side = 3; side <= sideLength; side += 2) {
        let apart = side - 1;
        for(let i = 0; i < 4; i++) {
            lastDiagonal += apart;
            diagonals.push(lastDiagonal);
        }
    }
    return diagonals;
}
let result = calcDiagonals(1001);
let sum = result.reduce((acc, element) => acc += element);
//console.log(result);
console.log(sum);