//215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
//
//What is the sum of the digits of the number 21000?
open System
open System.Numerics

let sum = (BigInteger.Pow(new BigInteger(2), 1000)).ToString().ToCharArray() 
            |> Array.map (fun i -> Int32.Parse(i.ToString()))
            |> Array.fold (+) 0
printfn "%A" (sum)

