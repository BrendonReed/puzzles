//By replacing the 1st digit of the 2-digit number *3, it turns out that six of the 
//nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
//
//By replacing the 3rd and 4th digits of 56**3 with the same digit, this 
//5-digit number is the first example having seven primes among the ten 
//generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. 
//Consequently 56003, being the first member of this family, is the smallest 
//prime with this property.
//
//Find the smallest prime which, by replacing part of the number
//(not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
open System
#time
let sieve n =  
    let notDivisible i n = n % i <> 0L
    let rec sieveH originalCount i numbers =
        //quit when i > sqrt originalCount
        match (float i > sqrt (float originalCount)) with 
        | true -> numbers
        | false -> 
            //filter out numbers divisible by i
            let nextNumbers = 
                numbers |> List.filter (fun j -> (j <= i) || (notDivisible i j))
            let nextDivisor = nextNumbers |> List.find ((<) i) 
            sieveH originalCount nextDivisor nextNumbers
    (sieveH n 2L [2L..n])

let primes = sieve 2000000L

let checkPrime n = 
    let rec checkPrimeH n (i:int64) = 
        match i > (n / 2L) with 
        | true -> true
        | false ->
            match (n % i) = 0L with
            | true -> false
            | false -> checkPrimeH n (i+1L)
    checkPrimeH n 2L

let isprime i = checkPrime i

let replaceAt (input:string) index newChar =
    let chars = input.ToCharArray()
    chars.[index] <- newChar
    new string(chars)
//given a number iterate wildcards through it: 
//1 -> 0,1,2,3,4,5,6,7,8,9
//11 -> 01 11 21 31 41 51 61 71 81 91
//      10 11 12 13 14 15 16 17 18 19
//      00 11 22 33 44 55 66 77 88 99
//111 (*11) 011 111 211 311 411 511 611 711 811 911
//    (1*1) 101 111 121 131 141 151 161 171 181 191
//    (11*) 110 111 112 113 114 115 116 117 118 119
//    (1**) 111 122 133 144 155 166 177 188 199 100
//    (*1*) 111 212 313 414 515 616 717 818 919 010
//    (**1) 001 111 221 331 441 551 661 771 881 991
//    (***) 000 111 222 333 444 555 666 777 888 999
//


let familyHasXPrimes nonPrimeMax (n:int64) =
    let mutable nonPrimeCount = 0
    let rec has8 (i:int) (ns:string) =
        let ci = (i.ToString().ToCharArray()).[0]
        let x = replaceAt ns (ns.IndexOf("0")) ci
        let y = replaceAt x (x.IndexOf("0")) ci
        //let next = Int64.Parse(ns.Replace("0", i.ToString()))
        let next = Int64.Parse(y)
        nonPrimeCount <- if (not(isprime next)) then nonPrimeCount + 1 else nonPrimeCount
        if nonPrimeCount > nonPrimeMax || i = 9 then
            nonPrimeCount <= nonPrimeMax
        else
            has8 (i+1) ns
    has8 1 (n.ToString())

let withZeros i =
     i.ToString().IndexOf("0") <> i.ToString().LastIndexOf("0") && i < 2000000L

let r = 
    primes
    |> Seq.filter withZeros
    |> Seq.find (fun i -> (familyHasXPrimes 3 i ))
printfn "%A" (r)
#time
