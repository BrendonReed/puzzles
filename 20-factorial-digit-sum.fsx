//n! means n × (n − 1) × ... × 3 × 2 × 1
//
//For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
//and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
//
//Find the sum of the digits in the number 100!
open System
let s = List.fold (fun acc elem -> acc * elem) 1I [1I..100I]
printfn "%A" s
let digits = s.ToString().ToCharArray() |> Array.map (fun i -> Int32.Parse(i.ToString()))
let sum = Array.sum digits
printfn "%A" sum
