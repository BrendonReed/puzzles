
function primesBelowF(n) {
    //fill an array with all odd numbers below n and 2
    let initialList = [2];
    for(let i = 3; i < n; i+=2) {
        initialList.push(i);
    }

    //starting at bottom, cross out any that are multiples of X
    let lastDivisor = Math.sqrt(n);
    for(let toCrossOutIx = 1; toCrossOutIx < initialList.length; toCrossOutIx++) {
        let toCrossOut = initialList[toCrossOutIx];
        if (toCrossOut === null || (toCrossOut > lastDivisor)) {
            continue;
        }

        //since it's an inner loop, the speed difference for for vs foreach is significant ~1/3
        for(let i = 0; i < initialList.length; i++) {
            if (initialList[i] !== toCrossOut && initialList[i] % toCrossOut === 0) {
                initialList[i] = null;
            }
        }
    }
    return initialList.filter(i => i !== null);
}

function isPrimeBelow(number, primes) {
    for(let i = 0; i < primes.length; i++) {
        let thisPrime = primes[i];
        if (thisPrime > number) return false;
        if (thisPrime === number) return true;
    }
    return false;
}

export function PrimeHelper(primesBelow) {

    let primesToCheck = primesBelowF(primesBelow);
    return {
        isPrime: (number) =>  isPrimeBelow(number, primesToCheck),
        primesToCheck: primesToCheck
    }
}
