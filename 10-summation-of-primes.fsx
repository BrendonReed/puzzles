//The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.  //
//Find the sum of all the primes below two million.
#time
let sieve n =  
    let notDivisible i n = n % i <> 0L
    let rec sieveH originalCount i numbers =
        //quit when i > sqrt originalCount
        match (float i > sqrt (float originalCount)) with 
        | true -> numbers
        | false -> 
            //filter out numbers divisible by i
            let nextNumbers = 
                numbers |> List.filter (fun j -> (j <= i) || (notDivisible i j))
            let nextDivisor = nextNumbers |> List.find ((<) i) 
            sieveH originalCount nextDivisor nextNumbers
    (sieveH n 2L [2L..n])
let n = 2000000L
let primes = sieve n
//let s = primes |> List.sum
//let m = primes |> List.max
printfn "%A" (primes |> List.sum)
#time
